type storage = {
  names: map (int, string)
};
/* variant defining pseudo multi-entrypoint actions */
type action =
  | SetInfo ((int, string));

let setInfo = ((s, info): (storage, (int, string))): storage => {
  switch (Map.find_opt(info[0], s.names)) {
    | None => {...s, names: Map.add(info[0], info[1], s.names)}
    | Some (acc) =>  {...s, names: Map.update(info[0], Some (info[1]), s.names)}
  };
};
/* real entrypoint that re-routes the flow based on the action provided */
let main = ((p,storage): (action, storage)) => {
  let storage =
    switch (p) {
    | SetInfo (n) => setInfo((storage, n))
    };
  ([]: list(operation), storage);
};